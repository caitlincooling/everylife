//
//  TasksTableViewCell.swift
//  TechnicalTest
//
//  Created by David Taylor on 21/12/2018.
//  Copyright © 2018 everyLIFE Technologies. All rights reserved.
//

import UIKit

class TasksTableViewCell: UITableViewCell {

    // TODO A: Create this cell in the UITableView in Main.storyboard
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var typeIconImageView: UIImageView!
    
    func addTaskInformation(task: Task) {
        nameLabel.text = task.name
        descriptionLabel.text = task.description
        typeIconImageView.image = UIImage(named: task.type.rawValue)
    }
}
