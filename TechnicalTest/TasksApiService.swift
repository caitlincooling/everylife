//
//  TasksApiService.swift
//  TechnicalTest
//
//  Created by David Taylor on 21/12/2018.
//  Copyright © 2018 everyLIFE Technologies. All rights reserved.
//

import Foundation

struct Task: Codable {
    let id: Int
    let name: String
    let description: String
    let type: TaskType
}

enum TaskType: String, Codable {
    case general
    case medication
    case hydration
    case nutrition
}

protocol TasksApiServicing {
    
    func getTasks(onSuccess: @escaping ([Task]) -> ())
}

// NOTE: THIS CLASS SHOULD NOT BE MODIFIED UNDER ANY CIRCUMSTANCES
// Call the getTasks method from the service to help to return the tasks needed for the UI. This method has a built in delay of two seconds
// in order to simulate a slow network connection.

class TasksApiService: TasksApiServicing {
    
    private let operationQueue = OperationQueue()
    
    func getTasks(onSuccess: @escaping ([Task]) -> ()) {
        self.operationQueue.addOperation {
            let url = Bundle.main.url(forResource: "tasks", withExtension: "json")!
            let data = try! Data(contentsOf: url)
            let tasks = try! JSONDecoder().decode([Task].self, from: data)
            Thread.sleep(forTimeInterval: 2)
            OperationQueue.main.addOperation {
                onSuccess(tasks)
            }
        }
    }
}
